﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for Calculator
/// </summary>
[WebService(Namespace = "http://harbor-lab.ch/Calculator")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class Calculator : System.Web.Services.WebService
{
    #region Configuration

    private const string DB_CONNECTION_STRING = "SERVER=50.62.209.15; DATABASE=hl_calculator; UID=harbor-lab; PASSWORD=sv089224";

    private int basePrice = 125; //CHF

    private int maxPriceContractDuration = 3; //months

    private int minPriceContractDuration = 12; // months

    private Dictionary<string, int> OfficeLocations = new Dictionary<string, int> { { "rbtCustomer", 0 }, { "rbtOffice", 2 }, { "rbtOffshore", 1 } };

    private List<JSItem> WorkTypes = new List<JSItem>
                                    {
                                        new JSItem{ID="cbxWorkType1", ValuePoints=3, Header="User Design"},
                                        new JSItem{ID="cbxWorkType2", ValuePoints=2, Header="Web development"},
                                        new JSItem{ID="cbxWorkType3", ValuePoints=0, Header="PC Software development"},
                                        new JSItem{ID="cbxWorkType4", ValuePoints=1, Header="IT Support"},
                                        new JSItem{ID="cbxWorkType5", ValuePoints=2, Header="Testing"},
                                        new JSItem{ID="cbxWorkType6", ValuePoints=3, Header="Mobile application development"}
                                    };

    private List<JSItem> Technologies = new List<JSItem>
                                    {
                                        new JSItem{ID="cbxTechnology1", ValuePoints=3, Header="UI/UX design"},
                                        new JSItem{ID="cbxTechnology2", ValuePoints=0, Header=".NET"},
                                        new JSItem{ID="cbxTechnology3", ValuePoints=3, Header="Communication (WCF, COM)"},
                                        new JSItem{ID="cbxTechnology4", ValuePoints=2, Header="HTML, Javascript, CSS"},
                                        new JSItem{ID="cbxTechnology5", ValuePoints=4, Header="Mobile development"}
                                    };

    #endregion

    #region Web services

    [WebMethod]
    public string SaveFeedback(string name, string email, string company, string customerID, string message, bool sendEmail)
    {
        try
        {
            var nameConv = FixString(name);
            var emailConv = FixString(email);
            var companyConv = FixString(company);

            using (var mySqlConnection = new MySqlConnection(DB_CONNECTION_STRING))
            {
                mySqlConnection.Open();

                using (MySqlCommand cmdMySQL = mySqlConnection.CreateCommand())
                {
                    cmdMySQL.Connection = mySqlConnection;
                    cmdMySQL.CommandText = "UPDATE CustomerData SET Name=@Name, Email=@Email, Company=@Company, Message=@Message, Please_Contact=@Please_Contact WHERE Customer_ID=@Customer_ID";
                    cmdMySQL.Prepare();

                    cmdMySQL.Parameters.AddWithValue("@Name", nameConv);
                    cmdMySQL.Parameters.AddWithValue("@Email", emailConv);
                    cmdMySQL.Parameters.AddWithValue("@Company", companyConv);
                    cmdMySQL.Parameters.AddWithValue("@Message", message);
                    cmdMySQL.Parameters.AddWithValue("@Please_Contact", sendEmail.ToString());
                    cmdMySQL.Parameters.AddWithValue("@Customer_ID", customerID);
                    cmdMySQL.ExecuteNonQuery();
                }
            }
        }
        catch (Exception)
        {
            return "DB Access error.";
        }

        if (sendEmail)
        {
            try
            {
                var subject = string.Format("CALCULATOR FORM: contact customer with the ID [{0}]", customerID);
                var body = string.Format("Please contact the following customer in order to schedule a talk about a project :{0} Name: {1} Email: {2} Company: {3} Comment: {4} ID: {5}",
                    System.Environment.NewLine + System.Environment.NewLine,
                    name + System.Environment.NewLine,
                    email + System.Environment.NewLine,
                    company + System.Environment.NewLine,
                    message + System.Environment.NewLine,
                    customerID + System.Environment.NewLine);
                var address = "andrei.andriesi@harbor-lab.ch";
                var pass = "ibuSV089224";
                var smtp = new SmtpClient
                {
                    Host = "smtp.office365.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(address, pass),
                    Timeout = 20000
                };

                smtp.Send(address, "office@harbor-lab.ch", subject, body);
            }
            catch (Exception)
            {
                return "EMAIL error";
            }
        }

        return string.Empty;
    }

    [WebMethod]
    public string CalculatePrice(string months, string location, string workTypes, string technologies)
    {
        var mappedWorkMessage = MapJSONToList(workTypes, WorkTypes);
        if (!string.IsNullOrEmpty(mappedWorkMessage)) return mappedWorkMessage;

        var mappedTechMessage = MapJSONToList(technologies, Technologies);
        if (!string.IsNullOrEmpty(mappedTechMessage)) return mappedTechMessage;

        var price = CalculatePriceValue(months, location);
        var custumerIDMessage = SaveIdDB(months, location, workTypes, technologies, price);
        if (custumerIDMessage.EndsWith("ERROR")) return custumerIDMessage;

        var retValueObj = new object[] { PriceAsFriendlyName(price), custumerIDMessage };
        var js = new JavaScriptSerializer();

        return js.Serialize(retValueObj);
    }

    #endregion

    #region Private Methods - JSON Utilities

    private string MapJSONToList(string jsonMessage, List<JSItem> list)
    {
        ResetList(list);

        var javaScriptSerializer = new JavaScriptSerializer();
        var desListJSON = javaScriptSerializer.Deserialize<object[][]>(jsonMessage);
        if (desListJSON == null) return "JavaScriptSerializer.Deserialize ERROR";

        foreach (var item in desListJSON)
        {
            if (item == null) continue;
            if (item.Length != 2) continue;

            var value = item[0].ToString();
            var isEnabled = item[1].ToString();
            var listItem = list.FirstOrDefault(it => it.ID == value);
            if (listItem == null)
            {
                ResetList(list);
                return "ListItem not found ERROR";
            }

            if (isEnabled.ToLower() == bool.TrueString.ToLower())
            {
                listItem.IsChecked = true;
            }
        }

        return string.Empty;
    }

    private void ResetList(List<JSItem> list)
    {
        foreach (var item in list)
        {
            item.IsChecked = false;
        }
    }

    #endregion

    #region Private Methods - Calculations

    private string PriceAsFriendlyName(double price)
    {
        var value = price.ToString("C", CultureInfo.InvariantCulture);
        return value.Substring(1);
    }

    private double CalculatePriceValue(string months, string location)
    {
        var monthsValue = 0.0;
        if (!double.TryParse(months, out monthsValue)) return 0;

        var locationValuePoints = GetValuePointsOfficeLocation(location);
        var maxPrice = basePrice + locationValuePoints;

        foreach (var item in WorkTypes)
        {
            if (!item.IsChecked) continue;
            maxPrice += item.ValuePoints;
        }

        foreach (var item in Technologies)
        {
            if (!item.IsChecked) continue;
            maxPrice += item.ValuePoints;
        }

        var monthsValueInt = (int)monthsValue;
        var finalContractValue = 0.0;
        var values = "";
        var mhts = 0;

        for (int i = 1; i <= monthsValueInt; i++)
        {
            var value = GetMonthPriceValue(basePrice, maxPrice, maxPriceContractDuration, minPriceContractDuration, i);
            finalContractValue += value;
            mhts++;
            values += " / " + value;
        }

        var finalValue = Math.Round((double)finalContractValue / monthsValueInt, 2);

        return finalValue;
    }

    private int GetMonthPriceValue(int minPrice, int maxPrice, int minContractDuration, int nrMonths, int monthIndex)
    {
        if (nrMonths == minContractDuration) return maxPrice;
        if (monthIndex < minContractDuration) return maxPrice;
        if (monthIndex > nrMonths) return minPrice;

        var value = (maxPrice - minPrice) / (minContractDuration - nrMonths) * (monthIndex - minContractDuration) + maxPrice;
        return value < minPrice ? minPrice : value;
    }

    private int GetValuePointsOfficeLocation(string location)
    {
        if (OfficeLocations.ContainsKey(location))
        {
            return OfficeLocations[location];
        }

        return 0;
    }

    private string FixString(string value)
    {
        return value.Replace('"', ' ').Replace("'", string.Empty);
    }

    private string SaveIdDB(string months, string location, string workTypes, string technologies, double price)
    {
        var customerID = Guid.NewGuid();

        try
        {
            var locationConv = FixString(location);
            var workTypesConv = FixString(workTypes);
            var technologiesConv = FixString(technologies);
            var monthsConv = FixString(months);
            var priceConv = FixString(price.ToString());

            using (var mySqlConnection = new MySqlConnection(DB_CONNECTION_STRING))
            {
                mySqlConnection.Open();

                using (MySqlCommand cmdMySQL = mySqlConnection.CreateCommand())
                {
                    cmdMySQL.Connection = mySqlConnection;
                    cmdMySQL.CommandText = "INSERT INTO CustomerData (Req_Location, Req_WorkTypes, Req_Technologies, Req_Months, Price, Message, Please_Contact, Customer_ID) VALUES (@Req_Location, @Req_WorkTypes, @Req_Technologies, @Req_Months, @Price, @Message, @Please_Contact, @Customer_ID)";
                    cmdMySQL.Prepare();

                    cmdMySQL.Parameters.AddWithValue("@Req_Location", locationConv);
                    cmdMySQL.Parameters.AddWithValue("@Req_WorkTypes", workTypesConv);
                    cmdMySQL.Parameters.AddWithValue("@Req_Technologies", technologiesConv);
                    cmdMySQL.Parameters.AddWithValue("@Req_Months", monthsConv);
                    cmdMySQL.Parameters.AddWithValue("@Price", priceConv);
                    cmdMySQL.Parameters.AddWithValue("@Message", string.Empty);
                    cmdMySQL.Parameters.AddWithValue("@Please_Contact", string.Empty);
                    cmdMySQL.Parameters.AddWithValue("@Customer_ID", customerID.ToString());
                    cmdMySQL.ExecuteNonQuery();
                }
            }
        }
        catch (Exception e)
        {
            return e.Message + " ERROR";
        }

        return customerID.ToString();
    }

    #endregion
}
