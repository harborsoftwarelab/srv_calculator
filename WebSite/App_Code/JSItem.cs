﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for JSItem
/// </summary>
public class JSItem
{
    public string ID { get; set; }
    public string Header { get; set; }
    public bool IsChecked { get; set; }
    public int ValuePoints { get; set; }
}